﻿var $dynamics = [];
var alt = "✔";

$(document).ready(function () {
    $(".matrixRB").each(function () {
        $dynamics.push({ "RadioButton": $(this), "Label": $(this).next(), "Original": $(this).next().text() });
    });

    UpdateLabels();

    $(".matrixRB").change(function () {
        UpdateLabels();
    });

    $(window).resize(function () {
        UpdateLabels();
    });
});

function UpdateLabels()
{
    var match = window.matchMedia("(min-width : 768px)");
    if (match.matches) {
        $($dynamics).each(function () {
           if (this.RadioButton.is(":checked")) {
               this.Label.text(alt);
           }
           else {
                this.Label.text("");
           }
        });
    }
    else
    {
        $($dynamics).each(function () {
           this.Label.text(this.Original);
        });
    }
}