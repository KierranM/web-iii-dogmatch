﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DogMatcher.Models
{
    public class Dog
    {
        public string BreedName { get; set; }
        public string DisplayName { get; set; }
        public string ImageName { get; set; }
        [Required]
        public EScale ActivityLevel { get; set; }
        [Required]
        public EScale SheddingLevel { get; set; }
        [Required]
        public EScale GroomingLevel { get; set; }
        [Required]
        public EScale IntelligenceLevel { get; set; }
        [Required]
        public bool GoodWithChildren { get; set; }
        [Required]
        public bool Drools { get; set; }
        [Required]
        public ELength CoatLength { get; set; }
        [Required]
        public ESize Size { get; set; }

        public enum EScale
        {
            Low,
            Medium,
            High,
            NoPreference
        }

        public enum ELength
        {
            Short,
            Medium,
            Long
        }

        public enum ESize
        {
            Miniature,
            Small,
            Medium,
            Large,
            Giant
        }
    }
}